#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  const size_t lhs_length = strlen(lhs);
  const size_t rhs_length = strlen(rhs);

  const size_t result_length = (lhs_length > rhs_length ? lhs_length : rhs_length) + 1;
  char *result = (char *)malloc(result_length + 1);
  if (result == NULL) {
    debug_abort("Failed to allocate memory for result string\n");
  }

  for (size_t i = 0; i < result_length; ++i) {
    result[i] = '0';
  }
  result[result_length] = '\0';

  int carry = 0;
  for (size_t i = 0; i < result_length; ++i) {
    const int lhs_value = (i < lhs_length ? get_digit_value(lhs[lhs_length - i - 1]) : 0);
    const int rhs_value = (i < rhs_length ? get_digit_value(rhs[rhs_length - i - 1]) : 0);

    const int digit_sum = lhs_value + rhs_value + carry;

    carry = digit_sum / base;

    result[result_length - i - 1] = to_digit(digit_sum % base);
  }

  result = reverse((char *)drop_leading_zeros(result));

  return result;


  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Compute lhs - rhs assuming lhs > rhs
  const size_t max_length = strlen(lhs) + 1;
  char *result = calloc(max_length, sizeof(char));
  if (result == NULL) {
    debug_abort("Memory allocation failed");
  }

  size_t result_length = 0;
  int borrow = 0;
  for (size_t i = 0; lhs[i] != '\0' || rhs[i] != '\0'; ++i) {
    const int lhs_digit = get_digit_value(lhs[i]);
    const int rhs_digit = get_digit_value(rhs[i]);
    const int diff = lhs_digit - rhs_digit - borrow;
    if (diff < 0) {
      borrow = 1;
      result[result_length++] = to_digit(diff + base);
    } else {
      borrow = 0;
      result[result_length++] = to_digit(diff);
    }
  }

  result[result_length] = '\0';
  result = reverse(result);
  result = (char *)drop_leading_zeros(result);

  return result;

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  const size_t lhs_length = strlen(lhs);
  const size_t rhs_length = strlen(rhs);

  // Allocate memory to store the result string
  char *result = calloc(lhs_length + rhs_length + 1, sizeof(char));
  if (!result) {
    debug_abort("Failed to allocate memory for result in arithmatoy_mul()");
  }

  // Initialize the result string to '0'
  memset(result, '0', lhs_length + rhs_length);

  // Multiply each digit of lhs with each digit of rhs and add the result to the
  // appropriate position in the result string
  for (size_t i = 0; i < lhs_length; ++i) {
    const unsigned int lhs_digit_value = get_digit_value(lhs[lhs_length - i - 1]);
    unsigned int carry = 0;
    for (size_t j = 0; j < rhs_length; ++j) {
      const unsigned int rhs_digit_value = get_digit_value(rhs[rhs_length - j - 1]);
      const unsigned int product = lhs_digit_value * rhs_digit_value + carry + get_digit_value(result[i + j]);
      carry = product / base;
      result[i + j] = to_digit(product % base);
    }
    if (carry > 0) {
      const size_t position = i + rhs_length;
      result[position] = to_digit(carry);
    }
  }

  // Reverse the result string and drop any leading zeros
  reverse(result);
  return drop_leading_zeros(result);

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
