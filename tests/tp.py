# Aucun n'import ne doit être fait dans ce fichier

def nombre_entier(n: int) -> str:
    result = ""
    for i in range(n):
        result += "S"
    return result + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    result = b
    while a != "0":
        if a.startswith("S"):
            result = S(result)
            a = a[1:]
        else:
            raise ValueError("Invalid input")
    return result


def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    result = "0"
    for i in range(nombre_entier(int(b))):
        result = addition(result, a)
    if result[-1] == "0":
        result = result[:-1]
    if not all(c in "S" for c in result):
        raise ValueError(f"Invalid result: {result}")
    return result


def facto_ite(n: int) -> int:
    res = 1
    for i in range(2, n+1):
        res *= i
    return res


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        f0, f1 = 0, 1
        for i in range(2, n+1):
            fn = f0 + f1
            f0 = f1
            f1 = fn
        return fn


def golden_phi(n: int) -> int:
    phi = 1
    for i in range (1, n+1):
        phi = 1+1/phi
    return phi


def sqrt5(n: int) -> int:
    x = n/2
    eps = 1e-9
    while abs(x**5 - n) > eps:
        x = x - (x**5 - n)/(5*x**4)
    return round(x)


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    elif n == 1:
        return a
    elif n % 2 == 0:
        half = pow(a, n // 2)
        return half * half
    else:
        half = pow(a, (n - 1) // 2)
        return half * half * a
